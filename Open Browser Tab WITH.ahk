; A simple script that opens up your current tab in a different browser

; ENVIRONMENT
;------------------------------------------------
#NoEnv  ; Recommended for performance and compatibility with future AutoHotkey releases.
#SingleInstance, Force
SendMode Input  ; Recommended for new scripts due to its superior speed and reliability.
SetWorkingDir %A_ScriptDir%  ; Ensures a consistent starting directory.


; Icon taken from: https://github.com/PapirusDevelopmentTeam/papirus-icon-theme/tree/master/Papirus
Menu, Tray, Icon, %A_scriptdir%/icon.ico

;------------------------------------------------
; SETTINGS AND VARIABLES
;------------------------------------------------
; Filepaths of Browsers
Brave = C:\Users\%A_UserName%\AppData\Local\BraveSoftware\Brave-Browser\Application\brave.exe
BraveIncognito = %Brave% --incognito

Chrome = C:\Program Files\Google\Chrome\Application\chrome.exe
ChromeIncognito = %Chrome% --incognito

Firefox = C:\Program Files\Mozilla Firefox\firefox.exe
FirefoxPrivate = %Firefox% -private-window

Librewolf = C:\Program Files\LibreWolf\librewolf.exe
LibrewolfPrivate = %Librewolf% -private-window

; Pocket Settings
PocketAccessToken=
PocketConsumerKey=

; Telegram Settings
TelegramChatID=
TelegramBotToken=

; KDE Connect Settings
KDEConnectCLIExeFilepath=C:\Program Files\KDE Connect\bin\kdeconnect-cli.exe
KDEConnectDeviceOne=
KDEConnectDeviceTwo=
KDEConnectDeviceThree=

; yt-dlp Settings
AudioDownloadFolder = C:\Users\%A_UserName%\Downloads
VideoDownloadFolder = C:\Users\%A_UserName%\Downloads
VideoResolution = 720
AudioQuality = Best  ; Best OR MP3

; Trigger Within These Applications Only
GroupAdd, GroupOfBrowsers, ahk_exe firefox.exe
GroupAdd, GroupOfBrowsers, ahk_exe librewolf.exe
GroupAdd, GroupOfBrowsers, ahk_exe chrome.exe
GroupAdd, GroupOfBrowsers, ahk_exe brave.exe

; Trigger hotkeys only within these browsers, otherwise ignore shortcuts
#IfWinActive, ahk_group GroupOfBrowsers
;------------------------------------------------
; Hotkeys
;------------------------------------------------

; Key Shortcuts:
;Complete List can be found at: 
; https://autohotkey.com/docs/KeyList.htm#Advanced_buttons
;# : Windows
;+ : Shift
;^ : Ctrl
;! : Alt

; Open with Browser 
; Function arguments should match the variable that contains your browser filepath 
!f::OpenWithBrowser(Firefox) 
!+f::OpenWithBrowser(FirefoxPrivate) 

!b::OpenWithBrowser(Brave)
!+b::OpenWithBrowser(BraveIncognito)

!c::OpenWithBrowser(Chrome)
!+c::OpenWithBrowser(ChromeIncognito)

; Open with Archive.org
!a::OpenWithArchiveOrg("SameTab")
!+a::OpenWithArchiveOrg("NewTab")

; KDE Connect
!o::OpenOnKDEConnectDevice(KDEConnectDeviceOne, KDEConnectCLIExeFilepath)
!+o::OpenOnKDEConnectDevice(KDEConnectDevicTwo, KDEConnectCLIExeFilepath)

; yt-dlp 
!y::YouTubeDL("Video", VideoResolution, VideoDownloadFolder)
!+y::YouTubeDL("Audio", AudioQuality, AudioDownloadFolder)

; pocket
!p::SavePageToPocket(PocketConsumerKey, PocketAccessToken)

; Telegram
!m::SaveToTelegram(TelegramBotToken, TelegramChatID)

; Clipboard
!x::CopyURLToClipboard()

#if


; Functions
;------------------------------------------------
CopyURLToClipboard(){
	TabURL := GetURLofActiveTab()
	if(TabURL = "")
	Return

	Clipboard := TabURL

	ShowTooltip("Tab URL Copied to Clipboard Successfully", 1000)
	return
}

YouTubeDL(Type, Quality, DownloadFolder){
	TabURL := GetURLofActiveTab()
	if(TabURL = "")
	Return

	if(Type = "Video")
		Command = yt-dlp -f "bestvideo[height<=%Quality%]+bestaudio" --embed-thumbnail --add-metadata --no-mtime -o `"%DownloadFolder%\`%(title)s.`%(ext)s`" %TabURL%
	if(Type = "Audio"){
		if(Quality = "Best")
		command = yt-dlp -f bestaudio --extract-audio --embed-thumbnail --add-metadata --no-mtime -o `"%DownloadFolder%\`%(title)s.`%(ext)s`" %TabURL%
		else, 
		command = yt-dlp --extract-audio --audio-format mp3 --audio-quality 5 --no-mtime -o `"%DownloadFolder%\`%(title)s.`%(ext)s`" %TabURL%
	}

	run, %comspec% /C %command%
	Return
}

OpenWithArchiveOrg(TabOption){
	TabURL := GetURLofActiveTab()
	if(TabURL = "")
	Return

	NewURL = https://web.archive.org/web/2/%TabURL%

	if(TabOption = "SameTab"){
		send, %NewURL%
		send, {Enter}
	}

	if(TabOption = "NewTab"){
		send, ^t ; new tab
		sleep, 250
		send, %NewURL%
		send, {Enter}
	}
	return
}

SaveToTelegram(TelegramBotToken, TelegramChatID){
	TabURL := GetURLofActiveTab()
	if(TabURL = "")
	Return

	PostURL = https://api.telegram.org/bot%TelegramBotToken%/sendmessage?chat_id=%TelegramChatID%&text=%TabURL%
	PostStatus := URLDownloadToVar(PostURL)
	
	SuccessString = "message_id"
	if(!InStr(PostStatus, SuccessString)){
		MsgBox, Failed to Submit Post
		return
	}

	ShowTooltip("Tab Posted to Telegram Successfully", 1000)
	return
}


SavePageToPocket(PocketConsumerKey, PocketAccessToken){
	TabURL := GetURLofActiveTab()
	if(TabURL = "")
	Return

	url_str := "https://getpocket.com/v3/add"
	objParam := { "url" : TabURL                                                                              
	,"consumer_key" : PocketConsumerKey
	,"access_token" : PocketAccessToken }

	Status := SubmitJsonObject(url_str, objParam)

	SuccessStatusCode = response_code":"200"
	if(!InStr(Status, SuccessStatusCode)){
		MsgBox, Failed to Submit to Pocket
		return
	}

	ShowTooltip("Tab Saved to Pocket Successfully", 1000)
	return
}

ShowTooltip(Text, LengthOfTime){
	ToolTip, %Text%
	sleep, %LengthOfTime%
	ToolTip
	return
}

OpenOnKDEConnectDevice(DeviceID, KDEConnectCLIExeFilepath){
	TabURL := GetURLofActiveTab()
	if(TabURL = "")
	Return

	command = "%KDEConnectCLIExeFilepath%" -n "%DeviceID%" --share "%TabURL%"
	Clipboard := command
	Status := RunCMD(command)

	if(InStr(Status, "error") OR InStr(Status, "Couldn't find device")){
		MsgBox, Failed to Send URL.`nIs Device Connected?
		return
	}
	ShowTooltip("Tab Sent Successfully", 1000)

return
}


OpenWithBrowser(BrowserFilepath){
	TabURL := GetURLofActiveTab()
	if(TabURL = "")
	Return

	run, %BrowserFilepath% %TabURL%
}


GetURLofActiveTab(){
	ClipBackup := ClipboardAll ; Backup the clipboard contents
	Clipboard :=	; Empty clipboard
	send, {Escape} ; send the escape key to click out of ALT menu in case user is using an ALT+Letter shortcut

	Loop, 5 {
		send, ^l  ; Send control + l ; Default shortcut to select URL bar in most browsers
		sleep, 250
		send, ^c  ; send control + c
		ClipWait, .5 ; Wait for clipboard to get contents
		if(ErrorLevel){ ; if error, wait .5 seconds and try sending control + c again
			sleep, 500
			Continue
		}
		else, 
		break
	}
	send, {Escape} ; escape out of url bar activation

	TabURL := Clipboard

	Clipboard := ClipBackup ; restore clipboard contents
	Return TabURL
}




; Run commands through CMD without any popups
; used for KDEConnect functionality
RunCMD(CmdLine, WorkingDir:="", Codepage:="CP0", Fn:="RunCMD_Output") {  ;         RunCMD v0.94        
Local         ; RunCMD v0.94 by SKAN on D34E/D37C @ autohotkey.com/boards/viewtopic.php?t=74647                                                             
Global A_Args ; Based on StdOutToVar.ahk by Sean @ autohotkey.com/board/topic/15455-stdouttovar

  Fn := IsFunc(Fn) ? Func(Fn) : 0
, DllCall("CreatePipe", "PtrP",hPipeR:=0, "PtrP",hPipeW:=0, "Ptr",0, "Int",0)
, DllCall("SetHandleInformation", "Ptr",hPipeW, "Int",1, "Int",1)
, DllCall("SetNamedPipeHandleState","Ptr",hPipeR, "UIntP",PIPE_NOWAIT:=1, "Ptr",0, "Ptr",0)

, P8 := (A_PtrSize=8)
, VarSetCapacity(SI, P8 ? 104 : 68, 0)                          ; STARTUPINFO structure      
, NumPut(P8 ? 104 : 68, SI)                                     ; size of STARTUPINFO
, NumPut(STARTF_USESTDHANDLES:=0x100, SI, P8 ? 60 : 44,"UInt")  ; dwFlags
, NumPut(hPipeW, SI, P8 ? 88 : 60)                              ; hStdOutput
, NumPut(hPipeW, SI, P8 ? 96 : 64)                              ; hStdError
, VarSetCapacity(PI, P8 ? 24 : 16)                              ; PROCESS_INFORMATION structure

  If not DllCall("CreateProcess", "Ptr",0, "Str",CmdLine, "Ptr",0, "Int",0, "Int",True
                ,"Int",0x08000000 | DllCall("GetPriorityClass", "Ptr",-1, "UInt"), "Int",0
                ,"Ptr",WorkingDir ? &WorkingDir : 0, "Ptr",&SI, "Ptr",&PI)  
     Return Format("{1:}", "", ErrorLevel := -1
                   ,DllCall("CloseHandle", "Ptr",hPipeW), DllCall("CloseHandle", "Ptr",hPipeR))

  DllCall("CloseHandle", "Ptr",hPipeW)
, A_Args.RunCMD := { "PID": NumGet(PI, P8? 16 : 8, "UInt") }      
, File := FileOpen(hPipeR, "h", Codepage)

, LineNum := 1,  sOutput := ""
  While (A_Args.RunCMD.PID + DllCall("Sleep", "Int",0))
    and DllCall("PeekNamedPipe", "Ptr",hPipeR, "Ptr",0, "Int",0, "Ptr",0, "Ptr",0, "Ptr",0)
        While A_Args.RunCMD.PID and (Line := File.ReadLine())
          sOutput .= Fn ? Fn.Call(Line, LineNum++) : Line

  A_Args.RunCMD.PID := 0
, hProcess := NumGet(PI, 0)
, hThread  := NumGet(PI, A_PtrSize)

, DllCall("GetExitCodeProcess", "Ptr",hProcess, "PtrP",ExitCode:=0)
, DllCall("CloseHandle", "Ptr",hProcess)
, DllCall("CloseHandle", "Ptr",hThread)
, DllCall("CloseHandle", "Ptr",hPipeR)

, ErrorLevel := ExitCode

Return sOutput  
}


; -------------------------------
; CreateFormData - Creates "multipart/form-data" for http post
; Needed for Pocket Posting
; -------------------------------
SubmitJsonObject(url_str, objParam){
	CreateFormData(postData, hdr_ContentType, objParam)
	whr := ComObjCreate("WinHttp.WinHttpRequest.5.1")
	whr.Open("POST", url_str, true)
	whr.SetRequestHeader("Content-Type", hdr_ContentType)
	; whr.SetRequestHeader("User-Agent", "Mozilla/5.0 (Windows NT 6.1; WOW64; Trident/7.0; rv:11.0) like Gecko")                ; ???????
	whr.Option(6) := False ; No auto redirect
	whr.Send(postData)
	whr.WaitForResponse()
	json_resp := whr.ResponseText
	whr := 

	; Clipboard := json_resp                                             ; free COM object
	return json_resp

	; if(InStr(json_resp, "error_code"))
	; Return json_resp
}

; Used for WinHttp.WinHttpRequest.5.1, Msxml2.XMLHTTP ...
CreateFormData(ByRef retData, ByRef retHeader, objParam) {
	New CreateFormData(retData, retHeader, objParam)
}

; Used for WinInet
CreateFormData_WinInet(ByRef retData, ByRef retHeader, objParam) {
	New CreateFormData(safeArr, retHeader, objParam)

	size := safeArr.MaxIndex() + 1
	VarSetCapacity(retData, size, 1)
	DllCall("oleaut32\SafeArrayAccessData", "ptr", ComObjValue(safeArr), "ptr*", pdata)
	DllCall("RtlMoveMemory", "ptr", &retData, "ptr", pdata, "ptr", size)
	DllCall("oleaut32\SafeArrayUnaccessData", "ptr", ComObjValue(safeArr))
}

Class CreateFormData {

	__New(ByRef retData, ByRef retHeader, objParam) {

		CRLF := "`r`n"

		Boundary := this.RandomBoundary()
		BoundaryLine := "------------------------------" . Boundary

		; Loop input paramters
		binArrs := []
		For k, v in objParam
		{
			If IsObject(v) {
				For i, FileName in v
				{
					str := BoundaryLine . CRLF
					. "Content-Disposition: form-data; name=""" . k . """; filename=""" . FileName . """" . CRLF
					. "Content-Type: " . this.MimeType(FileName) . CRLF . CRLF
					binArrs.Push( BinArr_FromString(str) )
					binArrs.Push( BinArr_FromFile(FileName) )
					binArrs.Push( BinArr_FromString(CRLF) )
				}
			} 
			Else {
				str := BoundaryLine . CRLF
				. "Content-Disposition: form-data; name=""" . k """" . CRLF . CRLF
				. v . CRLF
				binArrs.Push( BinArr_FromString(str) )
			}
		}

		str := BoundaryLine . "--" . CRLF
		binArrs.Push( BinArr_FromString(str) )

		retData := BinArr_Join(binArrs*)
		retHeader := "multipart/form-data; boundary=----------------------------" . Boundary
	}

	RandomBoundary() {
		str := "0|1|2|3|4|5|6|7|8|9|a|b|c|d|e|f|g|h|i|j|k|l|m|n|o|p|q|r|s|t|u|v|w|x|y|z"
		Sort, str, D| Random
		str := StrReplace(str, "|")
		Return SubStr(str, 1, 12)
	}

	MimeType(FileName) {
		n := FileOpen(FileName, "r").ReadUInt()
		Return (n        = 0x474E5089) ? "image/png"
		: (n        = 0x38464947) ? "image/gif"
		: (n&0xFFFF = 0x4D42    ) ? "image/bmp"
		: (n&0xFFFF = 0xD8FF    ) ? "image/jpeg"
		: (n&0xFFFF = 0x4949    ) ? "image/tiff"
		: (n&0xFFFF = 0x4D4D    ) ? "image/tiff"
		: "application/octet-stream"
	}

}
;#############################################################################################################
; Update: 2015-6-4 - Added BinArr_ToFile()

BinArr_FromString(str) {
	oADO := ComObjCreate("ADODB.Stream")

	oADO.Type := 2 ; adTypeText
	oADO.Mode := 3 ; adModeReadWrite
	oADO.Open
	oADO.Charset := "UTF-8"
	oADO.WriteText(str)

	oADO.Position := 0
	oADO.Type := 1 ; adTypeBinary
	oADO.Position := 3 ; Skip UTF-8 BOM
	return oADO.Read, oADO.Close
}

BinArr_FromFile(FileName) {
	oADO := ComObjCreate("ADODB.Stream")

	oADO.Type := 1 ; adTypeBinary
	oADO.Open
	oADO.LoadFromFile(FileName)
	return oADO.Read, oADO.Close
}

BinArr_Join(Arrays*) {
	oADO := ComObjCreate("ADODB.Stream")

	oADO.Type := 1 ; adTypeBinary
	oADO.Mode := 3 ; adModeReadWrite
	oADO.Open
	For i, arr in Arrays
	oADO.Write(arr)
	oADO.Position := 0
	return oADO.Read, oADO.Close
}

BinArr_ToString(BinArr, Encoding := "UTF-8") {
	oADO := ComObjCreate("ADODB.Stream")

	oADO.Type := 1 ; adTypeBinary
	oADO.Mode := 3 ; adModeReadWrite
	oADO.Open
	oADO.Write(BinArr)

	oADO.Position := 0
	oADO.Type := 2 ; adTypeText
	oADO.Charset  := Encoding 
	return oADO.ReadText, oADO.Close
}

BinArr_ToFile(BinArr, FileName) {
	oADO := ComObjCreate("ADODB.Stream")

	oADO.Type := 1 ; adTypeBinary
	oADO.Open
	oADO.Write(BinArr)
	oADO.SaveToFile(FileName, 2)
	oADO.Close
}
; -------------------------------/CreateFormData - Creates "multipart/form-data" for http post-------------------------------
